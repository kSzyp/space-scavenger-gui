import React from "react";
import {ChoiceArea, Header, TextArea} from "../containers/index";
import {Slider} from "../components/index";

import borderPortrait from "../img/borderPortrait.png";
import borderRight from "../img/borderRight.png";

import "../components/_normalize.scss";
import {layout__container, borderPortrait__container, borderRight__container} from "./layout.module.scss";

const Layout = () => {

  return  (
    <div className={layout__container}>
      <Header/>
      <div className={borderPortrait__container}>
        <img src ={borderPortrait} alt ={"borderPortrait"}/>
      </div>

      <div className={borderRight__container}>
        <img src ={borderRight} alt ={"borderRight"}/>
      </div>

      <Slider side={"left"}> 
        <p>Some text here</p>
      </Slider>
      <Slider side={"right"}/>

      <TextArea/>
      <ChoiceArea/>
      

    </div>
  )
}

export default Layout;