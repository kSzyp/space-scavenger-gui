import React, { Fragment } from "react";
import {SliderButton} from "../index";
import { useState, useEffect, useRef } from "react";

import {slider__container, slider__container__left, slider__container__right, slider__box} from "./slider.module.scss";

const Slider = (props) => {
  const [colapsed, setColapsed] = useState(true);
  const sliderRef = useRef();

  const handleClickOutside = (event) => {
     if(sliderRef.current.contains(event.target)) {
        setColapsed(!colapsed)
    } else if(event.target !== sliderRef.current) {
      setColapsed(true)
    }
  }

  useEffect(() => {
    document.addEventListener("mouseup", handleClickOutside);
    return () => document.removeEventListener("scroll", handleClickOutside);
  }); 

  return  (
    <Fragment>
      <div ref={sliderRef} className={props.side === "left" ? `${slider__container} ${slider__container__left}` : `${slider__container} ${slider__container__right}`}
        style={props.side === "left" ? {transform: `translate(${colapsed ? '-220px' : '0'}, 0)`} : {transform: `translate(${colapsed ? '220px' : '0'}, 0)`}}>

        <div className={slider__box}>
          {props.children}
        </div>
        <SliderButton side={props.side} colapsed={colapsed}/>

      </div>
  </Fragment>
  )
}

export default Slider;