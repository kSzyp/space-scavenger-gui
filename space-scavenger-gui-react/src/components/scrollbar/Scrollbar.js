import React, { Fragment } from "react";
import scrollbar from "../../img/scroll.png";
import scrollButton from "../../img/scrollButton.png";
import {scrollbar__container, scrollable, scrollbar__wrapper, scrollbar__button, scrollbar__track} from "./scrollbar.module.scss";
import { useState, useEffect, useRef } from "react";

const Scrollbar = (props) => {

  const scrollbarRef = useRef();
  const childRef = useRef();
  const prevScrollY = useRef(0);
  const [goingUp, setGoingUp] = useState(false);
  const [scrollPosition, setScrollPosition] = useState(0);
  const [scrollAvailable, setScrollAvailable] = useState(false);

  const handleScroll = () => {
    const childScrollOffsetHeight = childRef.current.scrollHeight - childRef.current.offsetHeight;
    const currentScrollY = childRef.current.scrollTop;
    const availableScrollHeight = scrollbarRef.current.scrollHeight - 50;
    
    const scrollPercent = parseFloat((currentScrollY * 100 / childScrollOffsetHeight).toFixed(2));
    if (prevScrollY.current < currentScrollY && goingUp) {
      setGoingUp(false);
    }
    if (prevScrollY.current > currentScrollY && !goingUp) {
      setGoingUp(true);
    }
    prevScrollY.current = currentScrollY;
    setScrollPosition(availableScrollHeight * scrollPercent / 100)
  };

  useEffect(() => {
    setScrollAvailable(childRef.current.scrollHeight - childRef.current.offsetHeight > 0 ? true : false)
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [goingUp]); 

  const handleClick = (event) => {
    handleThumbPosition(event.clientY);
  }
  
  const handleThumbPosition = (cursorPosition) => {
    const childScrollOffsetHeight = childRef.current.scrollHeight - childRef.current.offsetHeight;
    const availableScrollHeight = scrollbarRef.current.scrollHeight;
    const elementOffset = parseInt(scrollbarRef.current.getBoundingClientRect().y.toFixed(0));
    const calculateScrollPosition = cursorPosition - elementOffset;
    const scrollPercent = parseFloat((calculateScrollPosition * 100 / availableScrollHeight).toFixed(2));

    childRef.current.scroll({top: childScrollOffsetHeight * scrollPercent / 100, behavior: 'auto'});
  }

  const handleTouchDown = (event) => {
    handleThumbPosition(event.touches[0].clientY);
  }

  const handleTouchUp = (event) => {
    handleThumbPosition(event.changedTouches[0].clientY);
  }

  const handleTouchMove = (event) => {
    handleThumbPosition(event.touches[0].clientY);
  }
  
  return  (
    <div className={scrollbar__container}>
      <div className={scrollbar__wrapper}>
        {scrollAvailable === true && 
          <Fragment>
            <img className={scrollbar__track} src={scrollbar} alt={"scrollbar"} onClick={handleClick} ref={scrollbarRef}/>
            <img className={scrollbar__button} src={scrollButton} alt={"scrollButton"} style={{top: `${scrollPosition}px`}} onTouchStart={handleTouchDown} onTouchEnd={handleTouchUp} onTouchMove={handleTouchMove}/>
          </Fragment>
        }
      </div>

      <div onScroll={handleScroll} className={scrollable} ref={childRef}>{props.children}</div> 
    </div>
  )
}

export default Scrollbar;