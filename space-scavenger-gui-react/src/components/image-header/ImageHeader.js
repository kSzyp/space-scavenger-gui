import React from "react";
import portrait from "../../img/portrait.png";

import {portrait__container} from "./imageHeader.module.scss";

const ImageHeader = () => {

  return  (
    <div  className={portrait__container}>
      <img src={portrait} alt={"portrait"}/>
    </div>
  )
}

export default ImageHeader;