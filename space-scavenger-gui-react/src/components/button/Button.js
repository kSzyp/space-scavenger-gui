import React from "react";
import acceptButton from "../../img/acceptButton.png";
import {button__container, button} from "./button.module.scss";

const Button = (props) => {

  return  (
    <div className={button__container}>
      <button className={button}>
        <img src={acceptButton} alt={"button"} />
        <span>{props.children}</span>
      </button>
    </div>
  )
}

export default Button;