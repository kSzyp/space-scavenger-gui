import React from "react";
import buttonLeft from "../../img/buttonLeft.png";
import buttonRight from "../../img/buttonRight.png";
import arrow from "../../img/arrow.png";

import {sliderButton__container, arrow__left, arrow__right, arrow__left__collapsed, arrow__right__collapsed, sliderButton, sliderButton__left, sliderButton_collapsed__left, sliderButton__right, sliderButton_collapsed__right} from "./sliderButton.module.scss";

const SliderButton = (props) => {
  return  (
    <div className={sliderButton__container}>

      <img src={props.side === "left" ? buttonLeft : buttonRight} alt={"sliderButton"} className={sliderButton}/>
      <img src={arrow} alt={"arrow"} 
        className={
          props.side === "left" && props.colapsed ? arrow__left__collapsed
          : props.side === "left" ? arrow__left
          : props.side === "right" && props.colapsed ? arrow__right__collapsed
          : arrow__right}/>
      
      <div className={
        props.side === "left" && props.colapsed ? `${sliderButton__left} ${sliderButton_collapsed__left}`
       : props.side === "left" ? sliderButton__left
       : props.side === "right" && props.colapsed ? `${sliderButton__right} ${sliderButton_collapsed__right}`
       : sliderButton__right}/>
  </div>
  )
}

export default SliderButton;