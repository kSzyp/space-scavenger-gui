import React, { Fragment } from "react";
import {headerTab__container, headerTab__active, headerTab__notActive, headerTab } from "./headerTab.module.scss";

import buttonActive from "../../img/buttonActive.png";
import buttonNotActive from "../../img/buttonNotActive.png";

const HeaderTab = (props) => {

  const activeTab = () => {
    props.setActive(props.index)
  }

  return  (
    <div className={headerTab__container} onClick={activeTab}>

      {props.active === true &&
        <img src={buttonActive} alt={"buttonActive"} className={headerTab__active}/>
      }

      {props.active === false &&
        <Fragment>
          <img src={buttonNotActive} alt={"buttonNotActive"} className={headerTab__notActive}/>
          <div className={headerTab} />
        </Fragment>
      }

      {props.children}
    </div>
  )
}

export default HeaderTab;