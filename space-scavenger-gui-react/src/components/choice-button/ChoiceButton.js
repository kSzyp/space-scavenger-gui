import React from "react";
import choiceButtonActive from "../../img/choiceButtonActive.png";
import choiceButtonNotActive from "../../img/choiceButtonNotActive.png";
import icon1 from "../../img/icon1.png";
import icon2 from "../../img/icon2.png";
import icon3 from "../../img/icon3.png";

import {choice__button, choice__button__content, choice__button__text, choice__button__image_icon, choice__button__image_bg} from "./choiceButton.module.scss";

const ChoiceButton = (props) => {

  return  (
    <div>
      <button className={choice__button}>
        <div className={choice__button__content}>
          <img className={choice__button__image_icon} src={props.index === 1 ? icon1 : props.index === 2 ? icon2 : icon3} alt={"buttonIcon"}/>
          <p className={choice__button__text}>
            {props.children}
          </p>
          <img className={choice__button__image_bg} src={props.active === true ? choiceButtonActive : choiceButtonNotActive} alt={"buttonBg"}/>
        </div>
      </button>
  </div>
  )
}

export default ChoiceButton;