import React from "react";
import {ChoiceButton, Button, Scrollbar} from "../../components/index";
import verticalDotLine from "../../img/verticalDotLine.png";
import horizontalDotLine from "../../img/horizontalDotLine.png";

import {choice__area__container, choice__area__vertical__left__line, choice__area__buttons__wrapper, choice__area__buttons__container, choice__area__vertical__right__line, choice__area__horizontal__line} from "./choiceArea.module.scss";

const ChoiceArea = () => {

  return (
    <div className={choice__area__container}>
      <img className={choice__area__vertical__left__line} src={verticalDotLine} alt={"verticalLine"}/>
      <img className={choice__area__vertical__right__line} src={verticalDotLine} alt={"verticalLine"}/>
      <img className={choice__area__horizontal__line} src={horizontalDotLine} alt={"horizontalLine"}/>
      
      <div className={choice__area__buttons__container}>
        <Scrollbar> 
          <div className={choice__area__buttons__wrapper}>
            <ChoiceButton active={true} index={1}>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. 
            </ChoiceButton>
            <ChoiceButton index={2}>
              Lorem ipsum dolor sit amet.
            </ChoiceButton>
            <ChoiceButton index={3}>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis partur
            </ChoiceButton>
          </div>
        </Scrollbar>
      </div>

      <Button>
        POTWIERDŹ
      </Button>
    </div>
  )
}

export default ChoiceArea;