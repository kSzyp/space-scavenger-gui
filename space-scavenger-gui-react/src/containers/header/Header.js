import React from "react";
import {ImageHeader, HeaderTab} from "../../components/index";
import { useState } from "react";
import {header__container, header__buttons_container} from "./header.module.scss";

const Header = () => {
  const [activeTab, setActiveTab] = useState(0);
  const tabs = ["PRZYGODA", "POSTAĆ", "EKWIPUNEK"];

  const handleActiveTab = (tabIndex) => {
    setActiveTab(tabIndex)
  }

  return  (
    <div  className={header__container}>
      <ImageHeader />

      <div className={header__buttons_container}>

        {tabs.map((tab, index) => (
          <HeaderTab active={activeTab === index ? true : false} setActive={handleActiveTab} key={tab} index={index}>
            <p>{tab}</p>
          </HeaderTab>
        ))}

      </div>

    </div>
  )
}

export default Header;