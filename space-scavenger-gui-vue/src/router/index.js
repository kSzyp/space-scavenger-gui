import Vue from "vue";
import VueRouter from "vue-router";
// import App from "@/App.vue";
import GameView from "@/views/gameView/GameView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: GameView
  },
  {
    path: "/options",
    name: "options",
    component: () => import("@/views/optionsView/OptionsView.vue"),
    meta: {
      visible: true
    }
  }
];

const router = new VueRouter({
  routes
});

export default router;
