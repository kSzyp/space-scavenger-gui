module.exports = {
  globDirectory: "dist/",
  globPatterns: ["**/*.{css,ico,png,html,js}"],
  swDest: "dist\\sw.js",
  swSrc: "src/service-worker.js"
};
